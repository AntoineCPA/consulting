<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="no" indent="no" media-type="text/html" />
  <?software_name FTF synchro?>
  <?software_version Version 3.5.000.203 MT/FOP 20020701/0.20.5.9?>
  <xsl:template match="/">
  <xsl:comment>CPA Memotech Data for HERE the <xsl:value-of select="//ActionReportParameter[ParamName='onDbDate']/ParamValue"/></xsl:comment>
    <xsl:copy-of select="."/>
  </xsl:template>
</xsl:stylesheet>