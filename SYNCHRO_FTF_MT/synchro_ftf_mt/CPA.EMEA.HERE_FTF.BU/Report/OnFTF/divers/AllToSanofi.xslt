<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="no" indent="yes" media-type="text/html"/>
	<?software_name PCT Online Filing?>
	<?software_version Version 3.5.000.203 MT/FOP 20020701/0.20.5.9?>
	<xsl:template match="/">
		<xsl:comment>Test nb : <xsl:value-of select="count(//Cases)"/>/<xsl:value-of select="count(//CaseName)"/> ou<xsl:value-of select="count(//ActionReport/CaseGlobalRelation[CountryKey='57'])"/>/<xsl:value-of select="count(//ActionReport/*)"/>
		</xsl:comment>
		
		<MySpecificRoot1>
			<xsl:for-each select="//Case">
				<ptf>
					<xsl:attribute name="id"><xsl:value-of select="./CASEKEY"/></xsl:attribute>
					<xsl:variable name="ptfCaseKey" select="./CASEKEY"/>
					<!-- Common case values :-->
					<!-- DEbug casekey : <xsl:value-of select="$currentCaseKey" ></xsl:value-of>-->
					<!---	Family reference !-->
					<xsl:comment>Family reference :</xsl:comment>
					<reference>
						<xsl:value-of select="./CASEREFERENCE"/>
					</reference>
					<!--	Family name/short patent title -	Case name/short patent title:-->
					<xsl:copy-of select="./SHORTTITLE"/>
					<patents>
						<xsl:for-each select="//Case[CASEKEY =  $ptfCaseKey]">
							<xsl:variable name="ptaCaseKey" select="./CASEKEY1"/>
							<patent>
								<xsl:attribute name="id"><xsl:value-of select="$ptaCaseKey"/></xsl:attribute>
								<xsl:comment>Patent reference :</xsl:comment>
								<reference>
									<xsl:value-of select="CASEREFERENCE1"/>
								</reference>
								<filingStatus/>
								<countryFiled>
									<xsl:value-of select="COUNTRYCODE"/>
								</countryFiled>
								<status>
									<xsl:value-of select="STATUSDESCRIPTION_1"/>
								</status>
								<xsl:if test="NUMBEROFCLAIMS">
									<NumberOfClaims>
										<xsl:value-of select="NUMBEROFCLAIMS"/>
									</NumberOfClaims>
								</xsl:if>
								<xsl:if test="NumberOfPages">
									<NumberOfClaims>
										<xsl:value-of select="NumberOfPages"/>
									</NumberOfClaims>
								</xsl:if>
								<!--		NUMBEROFCLAIMS
						SMALLENTITY 
PCTRECEIVECOUNTRYKEY 
PCTSEARCHCOUNTRYKEY-->
								<!---	

-	Case Type
-	Status (pending, granted, expired, abandoned)
-	European designated states (EP Patents only)
-	Applicant name
-	Description
-	Examination requested date
-	Last office action date
-	Number of OA's already happened
-	Total page numbers
-	Number of drawings pages
-	Number of claims
-	Number of claims pages
-	Number of separate claims
-	Number of drawing sheets
-	Number of office actions
-	-->
								<caseevents>
									<xsl:comment>Data about MT events</xsl:comment>
									<xsl:for-each select="//CaseEvent[CASEKEY = $ptaCaseKey] ">
										<xsl:variable name="ptaCaseEventKey" select="./CASEEVENTKEY"/>
										<xsl:comment>About : <xsl:value-of select="$ptaCaseEventKey"/>
<xsl:copy-of select="./EVENTKEY" ></xsl:copy-of>
<xsl:copy-of select="./EVENTDATE" ></xsl:copy-of>
<xsl:copy-of select="./EVENTNUMBER" ></xsl:copy-of>
										</xsl:comment>
										<xsl:if test="./EVENTKEY = 10">
											<xsl:comment>About FLG</xsl:comment>
											<filingDate>
												<xsl:value-of select="./EVENTDATE"/>
											</filingDate>
										</xsl:if>
										<xsl:if test="./EVENTKEY = 25">
											<xsl:comment>About GRT</xsl:comment>
											<grantDate>
												<xsl:value-of select="./EVENTDATE"/>
											</grantDate>
											<grantNumber>
												<xsl:value-of select="./EVENTNUMBER"/>
											</grantNumber>
										</xsl:if>
									</xsl:for-each>
									<!---	First filing type
-	First filing country
-	Priority country
-	Priority number
-	Priority date
-	First Filing Date/Earliest Priority Date 
-	Filing stage
-	Local filing date
-	Substantive Filing Date/International Filing Date
-	Filing date/International filing date
-	Application number
-	Publication number
-	Publication date
-	Allowance date
-	Grant date
-	Grant/Patent number 
-	Expiry date

-->
								</caseevents>
							</patent>
						</xsl:for-each>
					</patents>
					<Keywords>
						<!--Keywords -->
					</Keywords>
				</ptf>
			</xsl:for-each>
		</MySpecificRoot1>
	</xsl:template>
</xsl:stylesheet>
