﻿using CPA.BusinessFacade;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPA.EMEA.HERE_FTF.BU
{
    public class Business
    {
        public string GetDate()
        {
            try
            {

                //LocalSetting.WritMsg("Ds {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string result = string.Empty;

                //string procName = string.Format("GTW_INSERM_GRC.GetCurrentDate", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string procName = @"FTF_SYNCHRO.GetCurrentDate";

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       new Hashtable());

                result = dt.Tables[0].Rows[0][0].ToString();

                return result;

            }
            catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);

                throw;
            }

        }

        public string CreateExport(string targetFileName = "hereData.xml" )
        {
            try
            {
                string result = string.Empty;
                //string procName = string.Format("GTW_INSERM_GRC.GetCurrentDate", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string procName = @"FTF_SYNCHRO.CreateExport";
                //
                Hashtable ht = new Hashtable();
                ht.Add("oFileName", targetFileName);

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       ht);

                result = dt.Tables[0].Rows[0][0].ToString();

                return result;

            }
            catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);
                throw;
            }
        }
    }
}
