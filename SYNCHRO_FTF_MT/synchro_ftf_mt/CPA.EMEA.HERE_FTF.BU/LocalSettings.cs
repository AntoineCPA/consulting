﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPA.EMEA.HERE_FTF.BU
{
    public static class LocalSetting
    {
        public static string GetValue(string key, string defaultValue = "")
        {
            string retour = System.Configuration.ConfigurationManager.AppSettings.Get(key);

            if (string.IsNullOrEmpty(retour))
                return defaultValue;

            return retour;

        }

        public const string WS_USERIDENTITYKEY = "1";

        static public void WritMsg(string txt, params string[] tabs)
        {
            if (!string.IsNullOrEmpty(LocalSetting.GetValue("logFile")))
            {
                System.IO.File.AppendAllText(LocalSetting.GetValue("logFile"), string.Format("{0}:\t{1}\r\n", DateTime.Now.ToString(), string.Format(txt, tabs))) ;
            }
            else {
                Console.WriteLine(txt, tabs);
            }
        }

        public static string SPListName = "Inventions";

        public static string SpDateNeededViewId
        {
            get
            {
                return GetValue("SPS_viewMTdataId", @"0A93C54C-D658-432D-BBD8-CCC646B2E34F");
            }
        }

        public static string CGG_Login
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["CGG_Login"];
            }
        }

        public static string CGG_Password
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["CGG_Password"];
            }
        }

        public static string CGG_SPSurl
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["CGG_Password"];
            }
        }
    }
}
