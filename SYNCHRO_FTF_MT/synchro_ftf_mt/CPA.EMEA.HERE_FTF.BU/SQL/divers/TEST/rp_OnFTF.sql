PROMPT CPAmemotech-Report-Proc-rp_OnFTF
PROMPT Release 5.3.3848

CREATE OR REPLACE PROCEDURE rp_OnFTF (
   psUserIdentityKey   IN     VARCHAR2,
   psCulture           IN     VARCHAR2,
   psXmlParameters     IN     VARCHAR2,
   pcCurList1             OUT CPAmemotechType.RefCur)
IS
   RELEASE_5_3_3848   CONSTANT BOOLEAN := FALSE;

   xpParams                    xmlparser.Parser;
   xnParams                    xmldom.DOMNode;

   nCaseEvaluationKey          INTEGER;
   sQuery                      VARCHAR2 (4000);
   sDomainRestriction          VARCHAR2 (4000);
BEGIN
   OPEN pcCurList1 FOR
    select * from FTF_EXCHANGE where ASKSEND = '0' ;
   
    update FTF_EXCHANGE set ASKSEND = '1',dateSended = sysdate where  ASKSEND = '0' ;
    commit;      
	and pca.ConfirmationNumber = 'FTF'

END rp_OnFTF;
/