/* Formatted on 17/01/2017 11:02:12 (QP5 v5.300) */
-----------------------------
-----------------------------
--AEC Package to synchro FTF
-- 17/01/2017
-----------------------------

CREATE OR REPLACE PACKAGE FTF_SYNCHRO
AS
    PROCEDURE GetCurrentDate (
        psUserIdentityKey   IN     VARCHAR2,
        psCulture           IN     VARCHAR2,
        psXmlParameters     IN     VARCHAR2,
        pcResult               OUT CPAmemotechType.RefCur);

    PROCEDURE CreateExport (
        psUserIdentityKey   IN     VARCHAR2,
        psCulture           IN     VARCHAR2,
        psXmlParameters       IN     VARCHAR2,
        pcResult               OUT CPAmemotechType.RefCur);
        
  PROCEDURE LogTest (psDescription VARCHAR2 DEFAULT NULL);        
        
END;

/
CREATE OR REPLACE PACKAGE BODY FTF_SYNCHRO
AS
    PROCEDURE GetCurrentDate (
        psUserIdentityKey   IN     VARCHAR2,
        psCulture           IN     VARCHAR2,
        psXmlParameters     IN     VARCHAR2,
        pcResult               OUT CPAmemotechType.RefCur)
    IS
    BEGIN
        OPEN pcResult FOR SELECT SYSDATE FROM DUAL;
    END GetCurrentDate;

    PROCEDURE CreateExport (
        psUserIdentityKey   IN     VARCHAR2,
        psCulture           IN     VARCHAR2,
        psXmlParameters       IN     VARCHAR2,
        pcResult               OUT CPAmemotechType.RefCur)
    IS
       XmlParameters2   VARCHAR2 (2048);
       v_targetFileName VARCHAR2 (256);
       xmlData           XMLTYPE;
    BEGIN
        xmlData := xmltype (psXmlParameters);
    
        SELECT TRIM(EXTRACTVALUE (xmlData, '/parameters/parameter[@name="oFileName"]'))
        INTO v_targetFileName
        FROM DUAL;
        
        LogTest('v_targetFileName:'||v_targetFileName);
    
        XmlParameters2 := '<?xml version="1.0" encoding="iso-8859-1"?><parameters><parameter name="oFileName">'||v_targetFileName||'</parameter><parameter name="onDbDate">'||to_char(sysdate,'yyyy/mm/dd  HH24:MI:ss')||'</parameter></parameters>';
        
        LogTest('XmlParameters2:'||XmlParameters2);

        --                    '<?xml version="1.0" encoding="iso-8859-1"?><parameters>'||
        --                   '<parameter name="SendBy">aeckly@cpaglobal.com</parameter>'||
        --                   '<parameter name="SendTo">aeckly@cpaglobal.com</parameter>'||
        --                   '<parameter name="CopyTo"></parameter>'||
        --                   '<parameter name="Subject">Extract XML</parameter>'||
        --                   '<parameter name="Body" >Extract XML</parameter>'||
        --                   '</parameters>';
        INSERT INTO MailAwaiting (MailAwaiting.MailAwaitingKey,
                                  MailAwaiting.JobKey,
                                  MailAwaiting.ActionReportId,
                                  MailAwaiting.ActionReportParam)
             VALUES (NextKey.NEXTVAL,
                     1010,
                     'OnFTF',
                     XmlParameters2);

        COMMIT;
        
        OPEN pcResult FOR SELECT 1 FROM DUAL;
         
    END CreateExport;
    
    PROCEDURE LogTest (psDescription VARCHAR2 DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    psOwner        VARCHAR2 (256);
    psName         VARCHAR2 (256);
    pnLineNo       NUMBER;
    psCallerType   VARCHAR2 (256);
  BEGIN
    INSERT INTO APPLICATIONLOG (LOGKEY,
                                OWNERNAME,
                                OBJECTNAME,
                                DESCRIPTION,
                                LOGDATE,
                                PROCESSKEY,
                                JOBKEY,
                                TARGETREFERENCE)
    VALUES (SysAudit.NEXTVAL,
            USER,
            'FTFEXPORT',
            psDescription,
            SYSDATE,
            0,
            109,
            '-');
     COMMIT;
  END LogTest;
    
    
END FTF_SYNCHRO;
/

SHOW ERROR;