--
delete MailAwaiting ;
commit;

select to_char(sysdate,'yyyy/mm/dd  HH24:MI:ss') from dual ; 

select * from MailAwaiting ;

DECLARE
  XmlParameters VARCHAR2(2048);
BEGIN
  XmlParameters := '<?xml version="1.0" encoding="iso-8859-1"?><parameters><parameter name="oFileName">mydocTest.xml</parameter><parameter name="onDbDate">'||to_char(sysdate)||'</parameter></parameters>';
   
--                    '<?xml version="1.0" encoding="iso-8859-1"?><parameters>'||
--                   '<parameter name="SendBy">aeckly@cpaglobal.com</parameter>'||
--                   '<parameter name="SendTo">aeckly@cpaglobal.com</parameter>'||
--                   '<parameter name="CopyTo"></parameter>'||
--                   '<parameter name="Subject">Extract XML</parameter>'||
--                   '<parameter name="Body" >Extract XML</parameter>'||
--                   '</parameters>';
  INSERT INTO MailAwaiting (MailAwaiting.MailAwaitingKey, MailAwaiting.JobKey, MailAwaiting.ActionReportId, MailAwaiting.ActionReportParam) 
  VALUES 
  (NextKey.NEXTVAL,1010 , 'OnFTF', XmlParameters);
  COMMIT;
END;

select * from MailAwaiting ;

select MAILAWAITING.FAILURES from MailAwaiting ;