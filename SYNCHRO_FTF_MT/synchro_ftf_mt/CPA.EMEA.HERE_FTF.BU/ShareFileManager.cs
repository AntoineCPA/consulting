﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Web;
using System.IO;
using System.Collections;

namespace CPA.EMEA.HERE_FTF.BU
{
    public class ShareFileManager
    {
        public string hostname;
        public string username;
        public string password;

        public string clientId;
        public string clientSecret;
        public string localPath;
        public string targetFolderID;

        public ShareFileManager()
        {
            this.hostname = LocalSetting.GetValue("hostname");
            this.username = LocalSetting.GetValue("username");
            this.password = LocalSetting.GetValue("password");

            this.clientId = LocalSetting.GetValue("clientId");
            this.clientSecret = LocalSetting.GetValue("clientSecret");
            this.localPath = LocalSetting.GetValue("FileSourcePath");
            this.targetFolderID = LocalSetting.GetValue("targetFolderID");
        }
        private OAuth2Token _token = null;

        public OAuth2Token token { get
            {
                if (_token == null)
                {
                    Authenticate(this.hostname, this.clientId, this.clientSecret, this.username, this.password);
                }

                return _token;
            }
        }

        public void Authenticate(string hostname, string clientId, string clientSecret, string username, string password)
        {
            try {

                String uri = string.Format("https://{0}/oauth/token", hostname);
                Console.WriteLine(uri);

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("grant_type", "password");
                parameters.Add("client_id", clientId);
                parameters.Add("client_secret", clientSecret);
                parameters.Add("username", username);
                parameters.Add("password", password);

                ArrayList bodyParameters = new ArrayList();
                foreach (KeyValuePair<string, string> kv in parameters)
                {
                    bodyParameters.Add(string.Format("{0}={1}", HttpUtility.UrlEncode(kv.Key), HttpUtility.UrlEncode(kv.Value.ToString())));
                }
                string requestBody = String.Join("&", bodyParameters.ToArray());

                HttpWebRequest request = WebRequest.CreateHttp(uri);

                request.Credentials = System.Net.CredentialCache.DefaultCredentials;

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(requestBody);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //  Console.WriteLine(response.StatusCode);
                JObject token = null;
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    String body = reader.ReadToEnd();
                    token = JObject.Parse(body);
                }

                _token = new OAuth2Token(token);

            } catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);
                throw;
            }
        }




        public static void addAuthorizationHeader(HttpWebRequest request, OAuth2Token token)
        {
            request.Headers.Add(string.Format("Authorization: Bearer {0}", token.AccessToken));
        }

        public static string GetHostname(OAuth2Token token)
        {
            return string.Format("{0}.sf-api.com", token.Subdomain);
        }


        /// <summary>
        /// Does a multipart form post upload of a file to a url.
        /// </summary>
        /// <param name="parameterName">multipart parameter name. File1 for a standard upload.</param>
        /// <param name="file">the FileInfo to upload</param>
        /// <param name="uploadUrl">the url of the server to upload to</param>
        private void UploadMultiPartFile(string parameterName, FileInfo file, string uploadUrl)
        {
            try {
                string boundaryGuid = "upload-" + Guid.NewGuid().ToString("n");
                string contentType = "multipart/form-data; boundary=" + boundaryGuid;

                MemoryStream ms = new MemoryStream();
                byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundaryGuid + "\r\n");

                // Write MIME header
                ms.Write(boundaryBytes, 2, boundaryBytes.Length - 2);
                string header = String.Format(@"Content-Disposition: form-data; name=""{0}""; filename=""{1}""" +
                    "\r\nContent-Type: application/octet-stream\r\n\r\n", parameterName, file.Name);
                byte[] headerBytes = System.Text.Encoding.UTF8.GetBytes(header);
                ms.Write(headerBytes, 0, headerBytes.Length);

                // Load the file into the byte array
                using (FileStream source = file.OpenRead())
                {
                    byte[] buffer = new byte[1024 * 1024];
                    int bytesRead;

                    while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, bytesRead);
                    }
                }

                // Write MIME footer
                boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundaryGuid + "--\r\n");
                ms.Write(boundaryBytes, 0, boundaryBytes.Length);

                byte[] postBytes = ms.ToArray();
                ms.Close();

                HttpWebRequest request = WebRequest.CreateHttp(uploadUrl);
                request.Timeout = 1000 * 60; // 60 seconds
                request.Method = "POST";
                request.ContentType = contentType;
                request.ContentLength = postBytes.Length;
                request.Credentials = CredentialCache.DefaultCredentials;

                using (Stream postStream = request.GetRequestStream())
                {
                    int chunkSize = 48 * 1024;
                    int remaining = postBytes.Length;
                    int offset = 0;

                    do
                    {
                        if (chunkSize > remaining) { chunkSize = remaining; }
                        postStream.Write(postBytes, offset, chunkSize);

                        remaining -= chunkSize;
                        offset += chunkSize;

                    } while (remaining > 0);

                    postStream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Console.WriteLine("Upload Status: " + response.StatusCode);
                response.Close();

            }
            catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);

                throw;

            }
        }

        /// <summary>
        /// Uploads a File using the Standard upload method with a multipart/form mime encoded POST.
        /// </summary>
        /// <param name="localPath">the full path of the file to upload, like "c:\\path\\to\\file.name"</param>
        public void UploadFile(string localPath)
        {
            try
            {
                String uri = string.Format("https://{0}/sf/v3/Items({1})/Upload", GetHostname(token), this.targetFolderID);
                Console.WriteLine(uri);

                HttpWebRequest request = WebRequest.CreateHttp(uri);
                addAuthorizationHeader(request, token);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    String body = reader.ReadToEnd();

                    JObject uploadConfig = JObject.Parse(body);
                    string chunkUri = (string)uploadConfig["ChunkUri"];
                    if (chunkUri != null)
                    {
                        Console.WriteLine("Starting Upload");
                        UploadMultiPartFile("File1", new FileInfo(localPath), chunkUri);
                    }
                }


            }
            catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);

                throw;

            }
        }


        public void Delete(string itemId)
        {
            throw new NotImplementedException();

            try
            {

                String uri = string.Format("https://{0}/sf/v3/Items({1})", CPA.EMEA.HERE_FTF.BU.ShareFileManager.GetHostname(token), itemId);
                // WriteMsg(uri);

                HttpWebRequest request = WebRequest.CreateHttp(uri);

                CPA.EMEA.HERE_FTF.BU.ShareFileManager.addAuthorizationHeader(request, token);

                request.Method = "DELETE";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //   WriteMsg(response.StatusCode.ToString());
            }
            catch (Exception ex)
            {
                LocalSetting.WritMsg("Error in {0}, MSG: {1}{2}", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.InnerException == null ? string.Empty : "Inner Exception:" + ex.InnerException.Message);

                throw;

            }
        }

    }
}
