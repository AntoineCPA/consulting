﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace Service.SSO
{
    public class Saml2DecryptResponse
    {
        private XmlNamespaceManager _nsManager;
        private XmlDocument _xmlDoc;

        public Saml2DecryptResponse(XmlDocument xmlDocument)
        {
            _xmlDoc = xmlDocument;

            _nsManager = new XmlNamespaceManager(_xmlDoc.NameTable);
            _nsManager.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            _nsManager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
            _nsManager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
            _nsManager.AddNamespace("xenc", "http://www.w3.org/2001/04/xmlenc#");
        }

        public XmlNode GetDecryptedAssertion(X509Certificate2 myCert)
        {
            RSACryptoServiceProvider privateCsp = (RSACryptoServiceProvider)myCert.PrivateKey;

            // load the xmlDoc
            EncryptedXml encXml = new EncryptedXml(_xmlDoc);
            XmlElement encryptedDataElement = _xmlDoc.GetElementsByTagName("xenc:EncryptedData")[0] as XmlElement;
            EncryptedData encryptedData = new EncryptedData();
            encryptedData.LoadXml(encryptedDataElement);

            //get your cipher data from the encrypted assertion key info
            byte[] cipherBytes = GetKeyCipherValue();

            // use the RSACryptoServiceProvider to decrypt it  
            var symKey = privateCsp.Decrypt(cipherBytes, true);

            // get the assertion data
            byte[] dataCipherBytes = GetEncryptedAssertionData();

            // and the encryption method
            string encMethod = GetEncryptionMethod();

            // build your symmetric algorythm, used to decrypt your assertion data
            SymmetricAlgorithm symAlg = null;
            symAlg = GetAlgorithm(encMethod);
            symAlg.IV = encXml.GetDecryptionIV(encryptedData, encMethod);

            // decrypt the assertion data
            byte[] decryptedAssertionData = DecryptBytes(symAlg, dataCipherBytes, symKey, symAlg.IV);
            string rawText = Encoding.UTF8.GetString(decryptedAssertionData);

            // clean up the unencrypted text
            int samlStart = rawText.IndexOf("<saml:Assertion");
            int samlEnd = rawText.IndexOf("</saml:Assertion>") + 17 - samlStart;
            string cleanText = rawText.Substring(samlStart, samlEnd);

            // turn it into an xml element and return it
            XmlDocumentFragment fragment = _xmlDoc.CreateDocumentFragment();
            fragment.InnerXml = cleanText;
            return fragment;
        }

        public static byte[] DecryptBytes(SymmetricAlgorithm algorithm, byte[] encryptedData, byte[] keyBytes, byte[] iv)
        {
            byte[] plainTextBytes;

            int decryptedBytesCount;

            using (var decryptor = algorithm.CreateDecryptor(keyBytes, iv))
            {
                using (var memoryStream = new MemoryStream(encryptedData))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        plainTextBytes = new byte[encryptedData.Length];
                        decryptedBytesCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                        memoryStream.Close();
                        cryptoStream.Close();
                    }
                }
            }

            return plainTextBytes;
        }

        public byte[] GetKeyCipherValue()
        {
            var node = GetNode("//xenc:EncryptedKey//xenc:CipherData//xenc:CipherValue");
            return Convert.FromBase64String(node.InnerText);
        }

        public byte[] GetEncryptedAssertionData()
        {
            var node = GetNode("//xenc:EncryptedData//xenc:CipherData//xenc:CipherValue");
            return Convert.FromBase64String(node.InnerText);
        }

        public string GetEncryptionMethod()
        {
            XmlNode node = GetNode("//xenc:EncryptionMethod");
            return node.Attributes["Algorithm"].Value.Trim();
        }

        public XmlNode GetNode(string xpath)
        {
            return _xmlDoc.SelectSingleNode(xpath, _nsManager);
        }

        private static SymmetricAlgorithm GetAlgorithm(string symAlgUri)
        {
            SymmetricAlgorithm symAlg = null;

            switch (symAlgUri)
            {
                case EncryptedXml.XmlEncAES128Url:
                case EncryptedXml.XmlEncAES128KeyWrapUrl:
                    symAlg = SymmetricAlgorithm.Create("Rijndael");
                    symAlg.KeySize = 128;
                    symAlg.Padding = PaddingMode.None;
                    break;
                case EncryptedXml.XmlEncAES192Url:
                case EncryptedXml.XmlEncAES192KeyWrapUrl:
                    symAlg = SymmetricAlgorithm.Create("Rijndael");
                    symAlg.KeySize = 192;
                    break;
                case EncryptedXml.XmlEncAES256Url:
                case EncryptedXml.XmlEncAES256KeyWrapUrl:
                    symAlg = SymmetricAlgorithm.Create("Rijndael");
                    symAlg.KeySize = 256;
                    break;
                case EncryptedXml.XmlEncDESUrl:
                    symAlg = SymmetricAlgorithm.Create("DES");
                    break;
                case EncryptedXml.XmlEncTripleDESUrl:
                case EncryptedXml.XmlEncTripleDESKeyWrapUrl:
                    symAlg = SymmetricAlgorithm.Create("TripleDES");
                    break;
                default:
                    throw new ArgumentException("symAlgUri");
            }

            return symAlg;
        }
    }
}