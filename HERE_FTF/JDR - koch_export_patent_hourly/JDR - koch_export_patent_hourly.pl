#!/usr/bin/perl -w

use strict;
use DBI qw(:sql_types);
use HTTP::DAV;
use DateTime::Format::Strptime;
use utf8;
use XML::LibXML;
use XML::Twig;
use Compress::Zlib;
use Encode;

my $proxy = "165.225.34.41:10077";
$ENV{'HTTPS_PROXY'} = $proxy; 
$ENV{'HTTP_PROXY'} = $proxy;

$|=1;
$XML::LibXML::skipXMLDeclaration = 1;

$ENV{'LANG'}="en_US.UTF-8";
$ENV{'PERL_NET_HTTPS_SSL_SOCKET_CLASS'} = 'Net::SSL';
$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

my $dt_parser = new DateTime::Format::Strptime(pattern => "%F %T");
my $dt_formatter = new DateTime::Format::Strptime(pattern => "%d-%b-%Y");
my $fn = "koch_export_patents_hourly.xml";
		
my $url = "https://prod.firsttofile.com/webdav/Koch/imports/";
my $url2 = "https://prod.firsttofile.com/webdav/Koch/imports/ws_imp_script/";

my $d = HTTP::DAV->new();
$d->credentials( -user=>"USERNAME", -pass => 'PASSWORD', -url => $url, -realm => "Dav" );
$d->open( -url=>$url) or die ("can't open: " . $d->message . "\n");

sub dateConvert {
	if (!defined $_[0]) {
		return '';
	}
	my $dt = $dt_parser->parse_datetime($_[0]);
	return $dt_formatter->format_datetime($dt);
}

my $casequery = qq/
SELECT distinct
	c.LISTREGISTEREDOWNERS as assignee,
	substr(c.SHORTTITLE, 1, 100) as title,
	c.CURRENTSTATUSDESC as case_status,
	c.CASEREFERENCE as primary_case_nm,
	c.FILINGTYPEDESC as application_type,
	c.COUNTRYDESC as country,
	c.FAMILYDESC as case_family_nm,
	c.LISTKEYWORD23030526 as generic_field1,
	substr(c.listdivision, instr(c.listdivision, ';',-1) +1) as generic_field2,
	cf.PROGRAMPROJECTDESC as generic_field3,
	c.ORIGIN as generic_field4,
	(SELECT event.EVENTDESC FROM 
  		( SELECT ce.EVENTDUEDATE, ce.EVENTDESC, ce.casekey
			FROM QB_CASEEVENT ce
			WHERE ce.EVENTDATE is null and ce.EVENTDUEDATE is not null and ce.EVENTDUEDATE>sysdate
			ORDER BY ce.EVENTDUEDATE ASC 
      	) event
    WHERE c.casepatentapplicationkey = event.casekey and rownum=1) as generic_field5,
	c.US102NUMBER as confirmation_number,
	c.FILINGNUMBER as serial_number,
	c.FILINGDATE as file_date,
	c.GRANTNUMBER as patent_number,
	c.GRANTDATE as issue_date,
	(select listagg(td.DESCRIPTORVALUEDESCRIPTION, ';') WITHIN GROUP (ORDER BY 1)
	from casedescriptor cd join tabledescriptorvalue td on cd.descriptorvaluekey=td.descriptorvaluekey
	where c.familykey=cd.casekey) as cat_generic_field1,
	( select min(cp.FILINGDATE) 
    from QB_CASEPRIORITYPATENTS cp
    where c.casepatentapplicationkey=cp.prioritycasekey) as priority_date,
	c.LISTDIVISION as "group",
	(	select listagg(cf.BUSINESSLINEDESC, ';')
		WITHIN GROUP (ORDER BY 1) 
    	from QB_CASEPATENTFAMILY cf
    	where c.FamilyKey = cf.CasePatentFamilyKey) as tech_cluster,
	(select listagg(
		b.pdtstructure1desc || ';' || b.pdtstructure2desc || ';' || b.pdtstructure3desc || ';' || 
		b.pdtstructure4desc || ';' || b.pdtstructure5desc || ';' || b.pdtstructure6desc || ';' || 
		b.pdtstructure7desc || ';' || b.pdtstructure8desc || ';' || b.pdtstructure9desc || ';' || 
		b.pdtstructure10desc || ';' ||  b.pdtstructure11desc || ';' || b.pdtstructure12desc || ';' || 
		b.pdtstructure13desc || ';' ||  b.pdtstructure14desc || ';' || b.pdtstructure15desc, ';') 
	WITHIN GROUP (order by 1)
	FROM qb_casepdtstructure b
	where c.visiblecasekey=b.casekey) as products,	
	c.listinventors as inventors,
	c.ATTORNEYDESC as inhatt,
	c.ASSISTANTDESC as inhata,
	substr(c.PROCEDUREAGENTDESC,1,45) as outcou,
	substr(c.LOCALAGENTDESC,1,45) as outcoc,
	c.PUBLICATIONDATE as publication_date,
	c.PUBLICATIONNUMBER as case_publication_nm,
	(select max(acl.LOGDATE) from AUDITCASELOG acl where acl.casekey = c.casepatentapplicationkey) as cust_last_update,
	c.casetypekey,
	c.casepatentapplicationkey
FROM QB_CASEPATENTAPPLICATION c
	LEFT JOIN QB_CASEPATENTFAMILY cf on (c.FamilyKey = cf.CasePatentFamilyKey)
  LEFT JOIN sysaudittrail at on (c.casepatentapplicationkey=at.objectkey)
WHERE
((substr(c.listdivision,1,instr(c.listdivision, ';')-1)='Molex' and c.ISINFORCE=1)
	OR
		(
			(substr(c.listdivision,1,instr(c.listdivision, ';')-1)<>'Molex' or substr(c.listdivision,1,instr(c.listdivision, ';')-1) is null) and
			(
				EXISTS (SELECT distinct FAMILYDESC FROM QB_CASEPATENTAPPLICATION qc WHERE qc.ISINFORCE=1 and qc.FAMILYDESC=c.FAMILYDESC)
				or c.CURRENTSTATUSDESC = 'Grant'
				or (c.CURRENTSTATUSDESC = 'Abandoned' and (select max(acl.LOGDATE) from AUDITCASELOG acl where acl.casekey = c.casepatentapplicationkey) >= (sysdate-730))
			)
		)) and at.objectname='CASES' and at.auditEnd >= (sysdate-1\/24)
	/;
	
# my $user = 'KOCH';
# my $passwd = 'KOCH';
my $user = 'MEMOTECH';
my $passwd = 'M3m0Gr38t';

my $dbh1 = DBI->connect('dbi:ODBC:KOCH', $user, $passwd, { FetchHashKeyName => "NAME_lc"}) or die $DBI::errstr;
$dbh1->{LongReadLen}=10000; $dbh1->{LongTruncOk}=1;

my $casehandle = $dbh1->prepare($casequery) or die $dbh1->errstr;

my $tms = [];
$casehandle->execute() or die $dbh1->errstr;
my $fieldNames = $casehandle->{"NAME_lc"};
my $fieldNameHash = $casehandle->{"NAME_lc_hash"};

my $doc = XML::LibXML::Document->new('1.0', 'utf-8');
	
my $dumpfile = "$fn.gz";
my $outlog=gzopen($dumpfile,"wb");
$outlog->gzwrite("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<patents>\n");

while (my $aryref = $casehandle->fetchrow_hashref()) {
	while (my($key,$val) = each%{ $aryref }) {
		if (defined $val) {
			if ($key =~ /date$/ ) {
				$aryref->{$key} = dateConvert($val);
			} else {
				$aryref->{$key} = encode('utf-8',stripSpecialChar($val)); 
			}
		}
	}
	
	$aryref->{'case_family_nm'} =~ s/[^A-Za-z0-9\-\.\s]//gis;	
	$aryref->{'case_family_nm'} = trim($aryref->{'case_family_nm'});
	$aryref->{'primary_case_nm'} =~ s/[^A-Za-z0-9_\-\.\s\&\(\)\[\]\{\}]//gis;
	$aryref->{'primary_case_nm'}  =~ s/_/-/gs;
	$aryref->{'primary_case_nm'} =~ s/\/+/-/gis;

	my $patent = $doc->createElement("patent");
	
	my @flatfields = (
		'case_type',
		'primary_case_nm',
		'title',
		'case_status',
		'case_status_date',
		'application_type',
		'case_family_nm',
		'country',
		'case_suffix',
		'filing_jurisdiction',
		'confirmation_number',
		'file_date',
		'patent_number',
		'national_filing_date',
		'priority_date',
		'issue_date',
		'maintenance_status',
		'expire_date',
		'serial_number',
		'generic_field1',
		'generic_field2',
		'generic_field3',
		'generic_field4',
		'generic_field5',
		'generic_field6',
		'cust_last_update'
	);
	
	# fields for the prosecution area
	my @prosecutionfields = (
		'inhatt',
		'inhata',
		'inhata2',
		'outcou',
		'outcoc',
		'outcoc2',
		'examiner',
		'outside_counsel_docket_nm1',
		'outside_counsel_docket_nm2',
		'outside_counsel_docket_nm3',
	);

	# fields for the comments area
	my @commentfields = (
			'comment_generic_field1',
			'comment_generic_field2',
			'comment_generic_field3',
			'comment_generic_note1',
			'comment_generic_note2',
			'comment_generic_note3',
	);
	
	#filling all the flat nodes
	foreach (@flatfields) {
		#print $_ . "\n";
		my $node = &setFlatNode($_, $aryref);
		if (defined $node) {
			$patent->appendChild($node);
		}
	}
	
	#prosecution sub nodes.
	my $prosecution = $doc->createElement('prosecution');
	
	foreach (@prosecutionfields) {
		#print $_ . "\n";
		my $node = &setFlatNode($_, $aryref);
		if (defined $node) {
			$prosecution->appendChild($node);
		}
	}
	$patent->appendChild($prosecution);
		
	# Comments 
	my $comments = $doc->createElement('comments');
	# Comment fields
	foreach (@commentfields) {
		my $node = &setFlatNode($_, $aryref);
		if (defined $node) {
			$comments->appendChild($node);
		}
	}
	#add the entire comment tree to the doc
	$patent->appendChild($comments);	

	# Group Ownership
	my $category_ownership = $doc->createElement('category_ownership');
	
	#groups
	my $groups = $doc->createElement('groups');
	if ($aryref->{'group'}) {
		foreach my $group (split(/\s*;\s*/, $aryref->{group})) {
			$group =~ s/[\r\n#]//gs;
			$group = trim($group);

			if ($group) {   
            	my $node = $doc->createElement('group');
            	$node->appendTextNode($group);
            	$groups->appendChild($node);
        	}
		}		
	}

	$category_ownership->appendChild($groups);

	#technology_clusters
	my $technology_clusters = $doc->createElement('technology_clusters');
	if ($aryref->{'tech_cluster'}) {
		foreach my $tech_cluster (split(/\s*;\s*/, $aryref->{tech_cluster})) {
			$tech_cluster =~ s/[\r\n#]//gs;
			$tech_cluster = trim($tech_cluster);

			if ($tech_cluster) {   
            	my $node = $doc->createElement('technology_cluster');
            	$node->appendTextNode($tech_cluster);
            	$technology_clusters->appendChild($node);
        	}
		}		
	}
	$category_ownership->appendChild($technology_clusters);

	#products
	my $products = $doc->createElement('products');
	if ($aryref->{'products'}) {
		foreach my $product (split(/\s*;\s*/, $aryref->{products})) {
			$product =~ s/[\r\n#]//gs;
			$product = trim($product);

			if ($product) {   
            	my $node = $doc->createElement('product');
            	$node->appendTextNode($product);
            	$products->appendChild($node);
        	}
		}		
	}
	$category_ownership->appendChild($products);

	my $cat_generic_field1 = $doc->createElement('cat_generic_field1');
	if (defined $aryref->{'cat_generic_field1'}) {
		$cat_generic_field1->appendTextNode($aryref->{'cat_generic_field1'} );
	}
	$category_ownership->appendChild($cat_generic_field1);
	
	#add the entire category tree to the doc
	$patent->appendChild($category_ownership);

	# assignee information
	if (defined $aryref->{'assignee'} ) {
		my $assignments = $doc->createElement('assignments');
		my $assignment = $doc->createElement('assignment');
		
		# Split by \s, at most 2.
		my @assignmentnamearray = split(' ', $aryref->{'assignee'}, 2);
		
		$assignmentnamearray[0] = '' unless (defined $assignmentnamearray[0]);
		my $assignee_first_name = $doc->createElement('assignee_first_name');
		$assignee_first_name->appendTextNode($assignmentnamearray[0]);
		$assignment->appendChild($assignee_first_name);

		$assignmentnamearray[1] = '.' unless (defined $assignmentnamearray[1]);
		my $assignee_last_name = $doc->createElement('assignee_last_name');
		$assignee_last_name->appendTextNode($assignmentnamearray[1]);
		$assignment->appendChild($assignee_last_name);
		
		my $assignee_company_name = $doc->createElement('assignee_company_name');
		$assignee_company_name->appendTextNode($aryref->{'assignee'});
		$assignment->appendChild($assignee_company_name);
		
		my $assignor_first_name = $doc->createElement('assignor_first_name');
		$assignor_first_name->appendTextNode('Koch');
		$assignment->appendChild($assignor_first_name);

		my $assignor_last_name = $doc->createElement('assignor_last_name');
		$assignor_last_name->appendTextNode('internal assignor');
		$assignment->appendChild($assignor_last_name);
		
		my $assignor_company_name = $doc->createElement('assignor_company_name');
		$assignor_company_name->appendTextNode('Koch internal assignor');
		$assignment->appendChild($assignor_company_name);

		my $assignment_date = $doc->createElement('assignment_date');
		$assignment_date->appendTextNode('01-Jan-1900');
		$assignment->appendChild($assignment_date);
		
		$assignments->appendChild($assignment);
		$patent->appendChild($assignments);
	}

	# Inventors
	my $inventors = $doc->createElement('inventors');
	if (defined $aryref->{'inventors'} ) {
		foreach my $inventor (split(';', $aryref->{'inventors'})) {
			$inventor =~ s/[\r\n#]//gs;
			$inventor = trim($inventor);
			if (defined $inventor) {
            	my $node = $doc->createElement('inventor');
            	$node->appendTextNode($inventor);
            	$inventors->appendChild($node);
        	}
		}		
	}
	$patent->appendChild($inventors);
	
	# More complex sub trees manually built here.
	if (defined $aryref->{'case_publication_nm'} && defined $aryref->{'publication_date'}) {
		my $publications = $doc->createElement('publications');
		my $publication = $doc->createElement('publication');
		
		my $case_publication_nm = $doc->createElement('case_publication_nm');
		$case_publication_nm->appendTextNode($aryref->{'case_publication_nm'} );
		$publication->appendChild($case_publication_nm);
		
		my $publication_date = $doc->createElement('publication_date');
		$publication_date->appendTextNode($aryref->{'publication_date'} );
		$publication->appendChild($publication_date);
		
		my $publication_country_code = $doc->createElement('publication_country_code');
		$publication_country_code->appendTextNode('US');
		$publication->appendChild($publication_country_code);
		
		my $publication_kind_code = $doc->createElement('publication_kind_code');
		$publication_kind_code->appendTextNode('A1');
		$publication->appendChild($publication_kind_code);

		$publications->appendChild($publication);
		$patent->appendChild($publications);
	}

	$doc->setDocumentElement($patent);
	#print $aryref->{'casepatentapplicationkey'};
	#print "\n";
	my $xml = $doc->toString(1);
	$xml =~ s/^<\?xml[^>]+>\s*\n//s; 
	$outlog->gzwrite($xml);
	
}
$outlog->gzwrite("</patents>");
$outlog->gzclose();

$casehandle->finish();
$dbh1->disconnect();

if ($d->put(-local=> $dumpfile,-url=>$url)) {
	print "OK";
} else {
	print "err: " . $d->message . "\n";
}

if(defined $ARGV[0] && $ARGV[0] eq "-u") {
	if ($d->put(-local=>"$0",-url=>$url2)) {
		print "OK";
	} else {
		print "err: " . $d->message . "\n";
	}
}

exit;

sub setFlatNode{
	my($val) = $_[0];
	my($aryref) = $_[1];
	if (defined $aryref->{$val}) {
		my $tmpvar = $doc->createElement($val);
		my $dataval = $aryref->{$val};
		$dataval =~ s/\s+$//;
		$dataval =~ s/^\s+//;
		$tmpvar->appendTextNode($dataval);
		return $tmpvar
	}
	return;
}

sub trim {
    my($str) = @_;
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    return $str;
}

sub stripSpecialChar {
    my($string) = @_;
    $string =~ s/[^\x00-\x7F]//gis;
    return $string;
}