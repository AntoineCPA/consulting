﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Net;
using System.Web;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using CPA.EMEA.HERE_FTF.BU;
using System.Xml;
using System.Security.Cryptography.X509Certificates;

namespace CPA.EMEA.HERE_FTF.TESTWF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            this.tbxFilePath.Text = sfMan.localPath ;
            this.tbxFolderId.Text = sfMan.targetFolderID;

        }

        ShareFileManager sfMan = new ShareFileManager();


        private void btnTestReport_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {

                // TODO
                WriteMsg(new Business().CreateExport());



                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                    ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();

                WriteMsg("Error:" + ex.Message);
            }
        }

        /// <summary>
        /// btnTestDownload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTestShareFile_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();
            try
            {

             


                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch(Exception ex)
            {
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
                WriteMsg("Error:" + ex.Message);
            }
        }

        void WriteMsg(string msg)
        {
            tbxResult.Text += msg + "\r\n";
            tbxResult.Update();
        }

       

        /// <summary>
        /// btnTestAuth3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTestAuth3_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                sfMan.Authenticate(sfMan.hostname, sfMan.clientId, sfMan.clientSecret, sfMan.username, sfMan.password);


                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
                }
            catch (Exception ex)
            {
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
                WriteMsg("Error:" + ex.Message);

            }
        }

        private void btnTestUploadFirstTime_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                sfMan.UploadFile(tbxFilePath.Text);

                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();

                WriteMsg("Error:" + ex.Message);
            }
        }

        private void BtnDBBAccess_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {

                // TODO
                WriteMsg(new Business().GetDate());






                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();

                WriteMsg("Error:" + ex.Message);
            }
        }

        private void btnTestDecript_Click(object sender, EventArgs e)
        {

            string data1 = @"YL9hr0mnjl0vNHfl+/dgMinaSD3Zjj7WiiPTcFiyoAgTNL7cwAFP1hOnZiHOjxeblNToMF5+bK3y/Sv4/nB83BMQ+h0bmahlr+LeK+ZXtOvOJEyQaxrFlB6mH+iNkkKLozcA8B4LlAJkuRlL4605ssIGnM29s1x6SDqNceFHoGyyF+EsnegIkYwHMHkdho0CEDLLDxqDYcAUWruLH8VD9e1nbcHZfUmLboqcIyk7FITTA4qKf8n68Ve1bDdGVqEKXwcFQ4BZuU460LyHcMDJ+gT6MWWIT1p0XSzgQGxmiCpu15vANMI8MVk6OuW41oYH1QAz6qPUslQ9oBzBb3oZdA==";
            string data2 = @"Qc+Gq9mOhbCgONQZDnxZ4nTVVRubCYj0Ae+nlAUTfJh/6ZIH7yWXo/dzERvZN8IySb6gB9cwq7kU6ahpQQneKsyM0kqatJMgbmeccLJj+lF+11rVdnOqGM9nk6KN6DODF5Hta3m3T76UEyXpRRREKfF0vB2azaVZpYWUfv7GS6hYqz1WlRrX5q0FlxK8AaKKMZdZdn3d2+EDtWj64MiXuFutdT3maqsAQYvwyUS5x19U5cTSmMf/jXiMpbpgYiEakzvWXwf6II94j5CIduZUDCEv0627eJ+Yrsoh4IoIccNN49wNB6olfHLNL8u6Sye1OJ8Xowtr92/0OiM0MIvWukucbgAu9u3XMv0FgXly9iWBxc1+v8v0wVmcxwHA9OYn8LfYBBWMb1hkKODsbEc0rJUYCX1K3gSWRSfTIfkpSkPOxzPf+meqUMNrdkZn/tCdlXP5YCQ9Sch1WkGicRDJzna026/9C5nD7vj+WaVN2QXstuf41VUqd9BS9HWYMND+PQDNLjCR5pQgHqFgyoGH46bgxqqHW5jKpaKyhtiBvW5gwMnCijr6mq16/kZp3V+W9j/Txb3u/Gz9bq0Ueh55vU0BMAKQ+0SUjapCkSEU0/fHZuZff9AiD9hw9uu+KkpxuGfeyP8X6/cU2n3KmRs8+zRkhpkx2SXG+GdBFoOrEERTS/i6fV3OKDmPeQGm3O/73D0U5qXnB7St/9mkE9uO1BBPKWVulFVAbvg6BTrGq2ux7Hh3/qFy/JFEIOkSwZA3m/3HCyrwdmbVLJMvpNPHJd8ukKbn8LbQ/dKMbwW0bMNDja1afry0RhHyTnDP+mdAUO5JV3HvD8GrV9aVtamAT+M6yencT3sFSnBt4yE+jSlWgJwVVqlGpiOqNq9u8wWDIbkjCd9sPgbPFQIfCtlxmEykAIoVcggUSz+3dvHlVA83E2WrCS+EILZJguwIoSkqUGD60GLtpiZIIThQJYePgOKloiohPdjcw7DUnDzFcTZ+KES4SIG+3T7Mh1fOmajgww0svkxGeh7NloqxTT3Vx6CSh5b84x1JG6rv0FBm5mg0JZharnnogocpJQEjcy/XoPWbJZvHQbhpVtRDXYxMwuTNxcnmKjqTB5mpTaAmIZTv7QVFmEQKWdc7zBHUNlfoMNm6YIezuB53ac8/DdqYknuMO/wymb3b+boDaFL+OT2tAU+cQn29yk7S/GSzAGgIAN1GrtDCYGMtUTCTiBCpuoAEAvPEv/3GDObvPU8R1ftsRAfXYuUZlNWooEcp1kh7K5xEW5ZtZ3QST7eDSCTpXgf/BP1EbReXDuHW5VwJ9tiVg/CZkCh2GoH/nquFSN1HocPYCNB+EvGV4AOU1b8y98LTQC+C8R5miLEWNe0G1vp93qsEwH6zSxXHxzw+E61CcuCtamLPxHYdwTLbJDZKdwoLVAv8zI9zTGRtxxqNBCaOByb0P9QTf9N2fIvaYSGULSaEd6C659RX+gHAg6UauDdt44hoCqDOfzP7GKCVTu4yQbJDIareqhdwHRI8nCd6LiDospkzuD7KygL5YF9B4f33Y0oOUWHv1Z6Ufxmrh8rwc1dFN/RkuWT883+dF3c+HedI+OgzpL4mJmLE4wzmD9weGE/nzp6u+OMNgXVbgPNeW5oTGZKli+jCz5jMHvBfQfr9wrBl1KdlPQaKcz7O21taog99TY8Kcq9yD16jlAyQFMezJpF3H+lEAUUPWnAtdbQ1QrCVWWpJlZE25KdkL6tqOp1+2KkezraTZbbzwo+Ti4hnxTYVKLYvODr/qqCMYNCGHhu9+Est4deMMXRDN9Nqe2yPT+LhnBnL1G0SQQ+xiP+oo3ltErBEmnwnUURSop/+ZfLTeMAxpC5RB74yWW3e+ALqkr9zOewp/NeTsfkjv82HY2HmYP84ANjqEkAZW0FMl/4hFSGbNWtKUO7cm4HoHKaXGQk2lCkwniZDIheU/mhaw0lwxFx+iVTTwmtQbi0c9SSC5zHwt0yxr4zbmp8sJx3qKmOxI2gd5XFoL49VVuSlWeGbuKjkMcXdBZoYz5zxdlw31v07SZwfCL4y+0Ag1huumeq1QiJ7PW7Ndt1P4fa30O+mranmooyrKEybvBsRXPUPODfO91e9cJY1JClnAqscChOHLLIx7tAAXXcf3ChWnUKBHP9+n3Q7hyvZPEnb5dr+AkLDyPpbvN6DPYkPccpuay+kD3O5qa4oDudKax1NBtxtcBOLU/5xm4duUYKWlmTEY7n91JRzs+HxLvvBnSRG59PFfZ59QXgvOs+wUQnxAkOHGg02lONRDNxpnVIL0r2BR6DYQfpBlntT29hgsjQwr6he4i8TpM17WoXRvSBZZ3cLP4z/Jjit8YA6S+T9evvFp9yw99tWayUrs+uwr5+ck0QvtiHRHIAw0/VjEITVF+z4qv6DzoWfy8X6Q9GzKWgtDmlNCTgbXC2fCEZonexO71aw+qcreGPpS45fV/eKGXo+3bh8+ycgxt6hmxVNAwFrLLQohqNd24Th3i1UXINZ1OjH/w8f2bmYpvXMShvkk/Y6MRTVBcQ1Bo7Q7wMTPMxgjCsLOyp+wxXbrlz9seH8Y4XzeVppOIiaiK+7Hm3aVRaIHhn2LiqdzSLDWY+lIBRTa87maAZqnSA5I5on2H++qKb9+Eio97g26qjuoNpDEyD1afLzRE0LxjhoqykNI67N3kjash/wk/muUabgS6cKTumrbdJWpefGx9B0lu1Jl5F29xFw23Rhlpbr7KCG96tBTOnv1SBWcoj2bguoX+USveENiewOnLvhXqxx6mvGA8YJrD5vvOSa9R8gS5qv/KduhUrPf/QxKgb7vjZeJ8NmYDg1Ehr9UKedpFdm58Uarr0g6FWoVdnFt0/ZS7XWe/9G01xkRst3Vyd1Eyc5N29MFdwbAfKZmA8cKgooHkDLSKvXDGIASAn5bpfrwRQn22itudp+Zozbe4zuHQWCOry1VYsWMWWwxlfJH1b8ZNG7D9szAvdKgMmmBE9q+qo5xv20/wi5LNR98Qifjt5QcddiVa/QtnREoDiB8wxHR/mVFcHqDkKI00UmRyfpqvqppb7pnf77QguC5C01Q2f1xezbQEftw1cNMuES0ywAW2hd+0I4QQHfzwMaAHYUBtx+zIZxKPLsP8qqc8kZ8C+T6koh2VqOMNVBYun+kvCS+8ywRLmmuaVI4eU+s9MQmVccvZ0qluZ3dc2d5LNIqx2v1YP/sMcnZ3I+TTWiXSeqBQtlorupbEJ9+H9EOZjMApTa1Lz1ky21wYQ1g4BS4WqlRZHCEQtoP/rFJY5xJE8aC0jajn+xPMgCjQ4R4sw8BTD4GEr2He82PZd6XOycg+p9IYDn8W1uuRAJHP1sPb1k+GUcKRELYAsofgU/5mLRCUbuby8N3/UOY76O94Q9Jie2Ez5rhR1GlgOeDSCxmWz87Ah7ImFP5f0aFq3M4bCIvmssWHDa5w/4RHFGBweuKAbDqblfxs9US0+TP4ijgy6cgQngIo/eqtXmNF4uh0C01ttlt4/TdwpGFRBH0qATOmRFoJHRwuxlLAPa5SxUU8/7iksm15rjSSL0jV96bbvMDg4FfS/bCTpZ0d9p4JvRbFFR1n6cNDni6npJ5nE92uN4SyOi1MaQEFGei+LUnQvxtecTKZ1VbrH+pzzG8P0x2Mg4dIIhbW78anRhQ2IaGSOIcqmj36ksg9d/y2qDPUhbsZy+WjIIXHUkU3x9cSMxFEJRDSTpWYIkVuCA2wF+eTwO88drcmA4/mKdWNVOUZP9lEHyOn6QR5WlUnFjC+HewHWiqV392xCRRLdRIROFunMwsXEa2Han45dItCC+rU4VJpJzqZqVjfAWXBnZfLCBvcTuzSnt50rFfbUxLsBgAViTRUmIecSBoaTuiSvRAw97A87/R8jAjUWmSzp50vPyQVslCsyr85L3KIZPUP+kLLxhxeVzyJ8Y2hnNML2NIt/mzaPoS1oIu0ylLnobOkk6b5buzF45aWJkLWLcQup6AG8nl20URa5VSiSajoNAlVPeVvlHtBckGpQ8Qit4OLrcTgRsSCgwZ3amVTI1W4dvPZ6PnKgGnlM8XQze4JGmP2SF3yJXXotnLral8wScf4h6VnU/TlpBo5DOFwcltaK71W3uxuCEU4y4vDW2X0+o60a4yZ00ep6uKdFDdoLqsRqVWX/gq3OQdSCEeFY03z5YlFB5ipaB2T2wTRYZuj5a4KvhhcgBakaC8K8ao+anUOFSqPd7+r7A7vCxxnxtbEzLT/4REdAYX4lVm5M+6YYJa8WBvke62Q4NT+o/R4zWR6T28yYFW9UzE1d8SoeC9dFMcBATqjRfF9wWIT3/Vvwvn5GHivAxPSYscHFPEIlMixi/euezdxohOMo51buRLZ0YT8v1DnktsGeHYlJsadkgVoR8qFOGb43VZsSIFgC81oI8WjEIHh6pmdFr21YKyJZilPBkdy9aE1ZEfbTBooPKZ7iiK3wCGpWUna7frYpsY0+HjbuTnMNzllssNEuQUI4zHrO8Z2tSYnz+JRAACqvf7auyz/JTSExU/rFnHXnutLcPlLX0oaqlshWvf5WSBRQHkKZ68hE+0wsv95ZdUL/KUF+L+wRLCTEp6E1FYsO7vcwb2YwBTEZVCwL0kBiOnUm9lFryu6VLYGaenPX+pHTQbowW4VB+E5oGdhH3/kft7hJ1Hapwrq7TA87w88iKzy2ZfmUutQBs2IuRFMKVVhIhJVSQBEk/FezrEWZWGjXK8D5ahhL8qxH12N1ikqP+C42+wbYK8ADR8KgOO+EOWiK+KKzahRlG/Wf539He4U1adZsrC+REOetqHaTCl7Hs/r0HU3Zg/NEWoc0g1+qRU8VXJl7EgCxU8dCQUKgW2B4+/43SskoXcxwKToZwhPzjuiJp0fh1OT/XveEMZlH8MdESm5YUNDxmFaLt44O6c4Hs3EbL+zdszMjkAx7UIa3dJAm5Lf4Uf4y8EGgcU4lvi8Cd4kLoZjkFuAOMDUaKL0ksd+Is+9BXlwurKIa+hGNZZzFPa+oyygQP7u9t3DBCMVagEkBlfKRX8+DYhdEgK7jaAqSIswJK119fPjtZlfOS4VsHmeaIynzppxaKX0ZL0lBNjynsTA0bUWMlTraTvnt6R2AYclGLe6NsT9FkpvKKPSUeCnnMIGrvTRgDlj3m0LrMKeI1BnP2Ti1cjHKiNPMGcgE+Kdk5i0dvDbs2t/8SlX9C7XohFGVsxhoIBSUGahJ+aMbkFDj2gvBnm0qoBSzLdCg0tZDHEVvoSEc6SSAeiqF1RDqPwKEh9YFV1AawTEg6iH9E+R4FM9CHoyE/m6wKh1e+JXUlLELYLBCD4mbwfRBrVro3snW0FC9RhNlaKVs/lZ/f1MPdZKpkmwVY79OlEiTOJ9ASrbHPCx1jXBw//SbgsqhHE7P/8C5rQQ7bEA2WGS/oMywSoqXS+a1T5CJGTWr+xYdRIM35kX+Ha1zfF936Zec0504P+rMxzTiqKkPAJAFq9T/q3FP6A9aearWICotPds0XuqVVp8zDWJSsDbvSvBeFqYrtwXEh/inG4f8/5i0YDfmZ6yj9evVkrFkeeVJW3vSi0s05cKrLsxpYPNTDDuI=";

//            string key = @"MIIGZjCCBU6gAwIBAgIQR / 0dEWykJQofCGL1KhD70DANBgkqhkiG9w0BAQsFADB3MQswCQYDVQQG
//  EwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xHzAdBgNVBAsTFlN5bWFudGVjIFRy
//dXN0IE5ldHdvcmsxKDAmBgNVBAMTH1N5bWFudGVjIENsYXNzIDMgRVYgU1NMIENBIC0gRzMwHhcN
//MTYwODA0MDAwMDAwWhcNMTcwODIwMjM1OTU5WjCB2TETMBEGCysGAQQBgjc8AgEDEwJKRTELMAkG
//A1UEBhMCSkUxDzANBgNVBAgMBkplcnNleTESMBAGA1UEBwwJU3QgSGVsaWVyMR0wGwYDVQQPExRQ
//cml2YXRlIE9yZ2FuaXphdGlvbjEOMAwGA1UEBRMFOTM3NDMxGzAZBgNVBAoMEkNQQSBHTE9CQUwg
//TElNSVRFRDEbMBkGA1UECwwSQ1BBIEdsb2JhbCBIb3N0aW5nMScwJQYDVQQDDB50ZXN0LmV1Lm1l
//bW90ZWNoLmNwYWdsb2JhbC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCnoL5t
//I / jSSvRjONthFtv4yuBSm0AgE9gJFUSeTJx / BP + wLA2xdvUZ7ETj9Va7zK2uZzq8v7UXvxksYYlc
//6iLGmaUsHfNXkSUmsqlhwrfbmO5e6ugBV1gfw13jwwfGJqyKuKEL75I4LkOA7QKDiF5m2ff / 0N2g
//BoT0OX9YfK8Gyp4TIZUZ2Gh1aPH2JVcVSp0 + bBFnO5nv / pzckSuxaNkVtkAQDWEFFkzQHo5x5yzc
//xZwu / xoN8EJ9bDYy1DTnfS7EGhCKMRYlDEZNPJ4FdFtK9tyFGFqlDaJpblEJNssMlg48fXHFqcEo
//egCLIX0dPUcWfRwm0hYzY1yriQkpgWm7AgMBAAGjggKJMIIChTApBgNVHREEIjAggh50ZXN0LmV1
//Lm1lbW90ZWNoLmNwYWdsb2JhbC5jb20wCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBaAwbwYDVR0g
//BGgwZjBbBgtghkgBhvhFAQcXBjBMMCMGCCsGAQUFBwIBFhdodHRwczovL2Quc3ltY2IuY29tL2Nw
//czAlBggrBgEFBQcCAjAZDBdodHRwczovL2Quc3ltY2IuY29tL3JwYTAHBgVngQwBATArBgNVHR8E
//JDAiMCCgHqAchhpodHRwOi8vc3Iuc3ltY2IuY29tL3NyLmNybDAdBgNVHSUEFjAUBggrBgEFBQcD
//AQYIKwYBBQUHAwIwHwYDVR0jBBgwFoAUAVmr5906C1mmZGPWzyAHV9WR52owVwYIKwYBBQUHAQEE
//SzBJMB8GCCsGAQUFBzABhhNodHRwOi8vc3Iuc3ltY2QuY29tMCYGCCsGAQUFBzAChhpodHRwOi8v
//c3Iuc3ltY2IuY29tL3NyLmNydDCCAQQGCisGAQQB1nkCBAIEgfUEgfIA8AB2AN3rHSt6DU + mIIuB
//rYFocH4ujp0B1VyIjT0RxM227L7MAAABVlToIbEAAAQDAEcwRQIgRgJZf1VzwseS3mf / DdNYMQxi
//k2l + rPubGKOkRtfkqRECIQDZYvQqwUJtlVWHmMAsXoMkPFlVqvy / Veij6chS98twwAB2AKS5CZC0
//GFgUh7sTosxncAo8NZgE + RvfuON3zQ7IDdwQAAABVlToIdoAAAQDAEcwRQIgStm97ruAqI2WFDtR
//ahzbFrWcnBT / 7kjtR384kTZ8rt4CIQDZGNIMSUEj4Ww + 5rZxQUGAAkQ2KWvulA2X03dzYSC//zAN
//    BgkqhkiG9w0BAQsFAAOCAQEAPBY3G3j4E4rRS9eclvVOTSOHHZc7IthMk5zxOtepBklhkrbt239w
//GEMVAt9IVc2oaAFor2xzFf / Dnr4k9FsCb9XYUoC3B4TGAavISGf4MOvwCQEKQsiLRW7dfNyzrQJY
//URXEE4RXmlS2NVVnnUTVxjs4gzxQZ1TN8xWbyuXIArMBFjeLtxkgA0BEeIEUTnlZtM3gvQA / NnB2
//YqunIAMeOmDuU2TFzC0c0Z + Oll8LnCggdnsiYsZqd + XqIh9mU89wjj / 74hnEN / SzMLiYTk7QBmKs
//UakwqvBEkfJe2FAO60nkOZFGb0KfemmSrmVNtrnhYRZTzayhQeHHkSgHD8XUOg ==";

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(@"C:\GIT\HERE_FTF\CPA.EMEA.HERE_FTF.BU\CPA.EMEA.HERE_FTF.TESTWF\TestRequest.xml");

            Service.SSO.Saml2DecryptResponse samlDecript = new Service.SSO.Saml2DecryptResponse(xmlDocument);

            X509Certificate2 oX509Certificate2 = new X509Certificate2(@"C:\GIT\HERE_FTF\CPA.EMEA.HERE_FTF.BU\CPA.EMEA.HERE_FTF.TESTWF\Key1.pfx","truc",X509KeyStorageFlags.UserProtected);
            

            XmlNode el = samlDecript.GetDecryptedAssertion(oX509Certificate2);

            WriteMsg(el.InnerXml);




        }

        private void btnFTFTest_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            string testurl = @"" ;
            string login = "";
            string pwd = "";

            try
            {


            
                  ((Button)sender).BackColor = Color.Green;
            ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();

                WriteMsg("Error:" + ex.Message);
            }

        }
    }
}
