﻿using CPA.EMEA.HERE_FTF.BU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPA.EMEA.HERE_FTF.TESTWF
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            if (string.Compare(LocalSetting.GetValue("runType"), "WIN", true) == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            else
            {
                LocalSetting.WritMsg("Start");
                LocalSetting.WritMsg("Create report");

                string filePathName = LocalSetting.GetValue("FileSourcePath");
                string FileName = System.IO.Path.GetFileName(filePathName);

                if (System.IO.File.Exists(filePathName))
                    System.IO.File.Delete(filePathName);

                LocalSetting.WritMsg("Start CreateExport to FTF");

                Business bu = new Business();
                bu.CreateExport(FileName);

                System.Threading.Thread.Sleep(int.Parse(LocalSetting.GetValue("sleeptime")));

                int i = 1;
                while (!System.IO.File.Exists(filePathName) && i < 100 )
                {
                    System.Threading.Thread.Sleep(int.Parse(LocalSetting.GetValue("sleeptime")));
                    i++;
                }

                if (!System.IO.File.Exists(filePathName)) {
                    LocalSetting.WritMsg("Error: no Generation of {0}", filePathName);
                    return;
                }
                LocalSetting.WritMsg("Start upload to FTF");

                ShareFileManager shFilMan = new ShareFileManager();
                shFilMan.UploadFile(LocalSetting.GetValue("FileSourcePath"));
                
                LocalSetting.WritMsg("End");
            }
        }
    }
}
