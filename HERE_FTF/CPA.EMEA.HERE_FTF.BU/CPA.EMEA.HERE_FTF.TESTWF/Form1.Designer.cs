﻿namespace CPA.EMEA.HERE_FTF.TESTWF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestReport = new System.Windows.Forms.Button();
            this.tbxResult = new System.Windows.Forms.TextBox();
            this.btnTestUploadFirstTime = new System.Windows.Forms.Button();
            this.btnCreateToken = new System.Windows.Forms.Button();
            this.tbxFilePath = new System.Windows.Forms.TextBox();
            this.tbxFildId = new System.Windows.Forms.TextBox();
            this.tbxFolderId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnDBBAccess = new System.Windows.Forms.Button();
            this.btnTestDecript = new System.Windows.Forms.Button();
            this.btnFTFTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTestReport
            // 
            this.btnTestReport.Location = new System.Drawing.Point(9, 450);
            this.btnTestReport.Name = "btnTestReport";
            this.btnTestReport.Size = new System.Drawing.Size(169, 42);
            this.btnTestReport.TabIndex = 0;
            this.btnTestReport.Text = "btnTestReport";
            this.btnTestReport.UseVisualStyleBackColor = true;
            this.btnTestReport.Click += new System.EventHandler(this.btnTestReport_Click);
            // 
            // tbxResult
            // 
            this.tbxResult.Location = new System.Drawing.Point(210, 214);
            this.tbxResult.Multiline = true;
            this.tbxResult.Name = "tbxResult";
            this.tbxResult.ReadOnly = true;
            this.tbxResult.Size = new System.Drawing.Size(908, 404);
            this.tbxResult.TabIndex = 1;
            // 
            // btnTestUploadFirstTime
            // 
            this.btnTestUploadFirstTime.Location = new System.Drawing.Point(12, 281);
            this.btnTestUploadFirstTime.Name = "btnTestUploadFirstTime";
            this.btnTestUploadFirstTime.Size = new System.Drawing.Size(169, 41);
            this.btnTestUploadFirstTime.TabIndex = 3;
            this.btnTestUploadFirstTime.Text = "btnTestUploadFirstTime";
            this.btnTestUploadFirstTime.UseVisualStyleBackColor = true;
            this.btnTestUploadFirstTime.Click += new System.EventHandler(this.btnTestUploadFirstTime_Click);
            // 
            // btnCreateToken
            // 
            this.btnCreateToken.Location = new System.Drawing.Point(12, 214);
            this.btnCreateToken.Name = "btnCreateToken";
            this.btnCreateToken.Size = new System.Drawing.Size(169, 44);
            this.btnCreateToken.TabIndex = 4;
            this.btnCreateToken.Text = "btnCreateToken";
            this.btnCreateToken.UseVisualStyleBackColor = true;
            this.btnCreateToken.Click += new System.EventHandler(this.btnTestAuth3_Click);
            // 
            // tbxFilePath
            // 
            this.tbxFilePath.Location = new System.Drawing.Point(81, 63);
            this.tbxFilePath.Name = "tbxFilePath";
            this.tbxFilePath.Size = new System.Drawing.Size(350, 22);
            this.tbxFilePath.TabIndex = 5;
            // 
            // tbxFildId
            // 
            this.tbxFildId.Location = new System.Drawing.Point(81, 106);
            this.tbxFildId.Name = "tbxFildId";
            this.tbxFildId.Size = new System.Drawing.Size(350, 22);
            this.tbxFildId.TabIndex = 6;
            // 
            // tbxFolderId
            // 
            this.tbxFolderId.Location = new System.Drawing.Point(81, 143);
            this.tbxFolderId.Name = "tbxFolderId";
            this.tbxFolderId.Size = new System.Drawing.Size(350, 22);
            this.tbxFolderId.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "FilePath";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "FileId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "FolderId";
            // 
            // BtnDBBAccess
            // 
            this.BtnDBBAccess.Location = new System.Drawing.Point(12, 515);
            this.BtnDBBAccess.Name = "BtnDBBAccess";
            this.BtnDBBAccess.Size = new System.Drawing.Size(166, 53);
            this.BtnDBBAccess.TabIndex = 11;
            this.BtnDBBAccess.Text = "BtnDBBAccess";
            this.BtnDBBAccess.UseVisualStyleBackColor = true;
            this.BtnDBBAccess.Click += new System.EventHandler(this.BtnDBBAccess_Click);
            // 
            // btnTestDecript
            // 
            this.btnTestDecript.Location = new System.Drawing.Point(585, 63);
            this.btnTestDecript.Name = "btnTestDecript";
            this.btnTestDecript.Size = new System.Drawing.Size(181, 42);
            this.btnTestDecript.TabIndex = 12;
            this.btnTestDecript.Text = "btnTestDecript";
            this.btnTestDecript.UseVisualStyleBackColor = true;
            this.btnTestDecript.Click += new System.EventHandler(this.btnTestDecript_Click);
            // 
            // btnFTFTest
            // 
            this.btnFTFTest.Location = new System.Drawing.Point(15, 346);
            this.btnFTFTest.Name = "btnFTFTest";
            this.btnFTFTest.Size = new System.Drawing.Size(163, 58);
            this.btnFTFTest.TabIndex = 13;
            this.btnFTFTest.Text = "btnFTFTest";
            this.btnFTFTest.UseVisualStyleBackColor = true;
            this.btnFTFTest.Click += new System.EventHandler(this.btnFTFTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 630);
            this.Controls.Add(this.btnFTFTest);
            this.Controls.Add(this.btnTestDecript);
            this.Controls.Add(this.BtnDBBAccess);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxFolderId);
            this.Controls.Add(this.tbxFildId);
            this.Controls.Add(this.tbxFilePath);
            this.Controls.Add(this.btnCreateToken);
            this.Controls.Add(this.btnTestUploadFirstTime);
            this.Controls.Add(this.tbxResult);
            this.Controls.Add(this.btnTestReport);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTestReport;
        private System.Windows.Forms.TextBox tbxResult;
        private System.Windows.Forms.Button btnTestUploadFirstTime;
        private System.Windows.Forms.Button btnCreateToken;
        private System.Windows.Forms.TextBox tbxFilePath;
        private System.Windows.Forms.TextBox tbxFildId;
        private System.Windows.Forms.TextBox tbxFolderId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnDBBAccess;
        private System.Windows.Forms.Button btnTestDecript;
        private System.Windows.Forms.Button btnFTFTest;
    }
}

