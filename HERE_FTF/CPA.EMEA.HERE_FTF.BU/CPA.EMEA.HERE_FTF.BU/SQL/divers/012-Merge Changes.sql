--------------------------------------
-- Merge
--
--------------------------------------

--MERGE INTO employees e
--    USING hr_records h
--    ON (e.id = h.emp_id)
--  WHEN MATCHED THEN
--    UPDATE SET e.address = h.address
--  WHEN NOT MATCHED THEN
--    INSERT (id, address)
--    VALUES (h.emp_id, h.address);
----The source can also be a query :
--MERGE INTO employees e
--    USING (SELECT * FROM hr_records WHERE start_date > ADD_MONTHS(SYSDATE, -1)) h
--    ON (e.id = h.emp_id)
--  WHEN MATCHED THEN
--    UPDATE SET e.address = h.address
--  WHEN NOT MATCHED THEN
--    INSERT (id, address)
--    VALUES (h.emp_id, h.address);

-- Merge
-- test title, and case_status...

delete FTF_EXCHANGE ;
commit;

-- Add missing cases
MERGE INTO FTF_EXCHANGE FTF_excTarget
    using(
    select distinct
	substr(c.shorttitle,0,90) as title,
	c.casekey,
	ts.STATUSDESCRIPTION as case_status,
	c.casereference as primary_case_nm,
	tf.FILINGTYPEDESCRIPTION as application_type,
	tc.COUNTRYDESCRIPTION as country
FROM
	cases c
	LEFT JOIN CASEDESCRIPTOR cd on (c.casekey=cd.casekey)
	LEFT JOIN TABLEDESCRIPTORVALUE tdv on (cd.DESCRIPTORVALUEKEY=tdv.DESCRIPTORVALUEKEY 
											and tdv.DESCRIPTORKEY=4)
	JOIN GLOBALRELATION gr on (c.casekey=gr.relatedkey and gr.relationshipkey=104)
	join cases pc on (gr.parentkey=pc.casekey) -- pc: PriorityCase
	join caseevent cepc on (c.casekey=cepc.casekey and cepc.EVENTKEY=10) --cepc: CaseEventPriorityCase
	LEFT JOIN tablestatus ts on (c.statuskey=ts.statuskey)
	LEFT JOIN TABLEFILINGTYPE tf on (c.FILINGTYPEKEY=tf.FILINGTYPEKEY)
	LEFT JOIN tablecountry tc on (c.COUNTRYKEY=tc.COUNTRYKEY)
	left join casename cn106 on (c.casekey = cn106.casekey and cn106.nametypekey=106)
	left join name n106 on (n106.namekey = cn106.namekey)
	left join casename cn105 on (c.casekey = cn105.casekey and cn105.nametypekey=105)
	left join name n105 on (n105.namekey = cn105.namekey)
	left join casename cn101 on (c.casekey = cn101.casekey and cn101.nametypekey=101)
	left join name n101 on (n101.namekey = cn101.namekey)
	left join casename cn504 on (c.casekey = cn504.casekey and cn504.nametypekey=504)
	left join name n504 on (n504.namekey = cn504.namekey)
	left join casename cn12 on (c.casekey = cn12.casekey and cn12.nametypekey=12)
	left join name n12 on (n12.namekey = cn12.namekey)
	left join caseevent ce10 on (c.casekey=ce10.casekey and ce10.EVENTKEY=10)
	left join caseevent ce15 on (c.casekey=ce15.casekey and ce15.EVENTKEY=15)
	left join caseevent ce19 on (c.casekey=ce19.casekey and ce19.EVENTKEY=19)
	left join caseevent ce30 on (c.casekey=ce30.casekey and ce30.EVENTKEY=30)
	left join caseevent ce50 on (c.casekey=ce50.casekey and ce50.EVENTKEY=50)
WHERE
	c.propertytypekey = 1 
	and c.casetypekey = 1 
	and not c.statuskey in (3, 5811488)
	and c.casekey in 
		(select distinct casekey from auditcaselog inner join 
		auditlogtype at on at.auditlogtypekey = auditcaselog.auditlogtypekey 
		where auditlogcode like 'Case%')
) FTF_source-- end using
ON (FTF_excTarget.casekey = FTF_source.casekey)
WHEN MATCHED THEN
update SET ASKSEND = '2'
WHEN NOT MATCHED THEN
 INSERT (TITLE,CASE_STATUS,PRIMARY_CASE_NM,APPLICATION_TYPE,COUNTRY,casekey,DATEUPDATED,ASKSEND) VALUES (FTF_source.TITLE,FTF_source.CASE_STATUS,FTF_source.PRIMARY_CASE_NM,FTF_source.APPLICATION_TYPE,FTF_source.COUNTRY,FTF_source.casekey,sysdate,'0')
 ;

select TITLE,CASE_STATUS,PRIMARY_CASE_NM,APPLICATION_TYPE,COUNTRY,casekey,DATEUPDATED,ASKSEND from FTF_EXCHANGE ;



MERGE INTO FTF_EXCHANGE FTF_excTarget
    using(
    select distinct
	substr(c.shorttitle,0,90) as title,
	c.casekey,
	ts.STATUSDESCRIPTION as case_status,
	c.casereference as primary_case_nm,
	tf.FILINGTYPEDESCRIPTION as application_type,
	tc.COUNTRYDESCRIPTION as country
FROM
	cases c
	LEFT JOIN CASEDESCRIPTOR cd on (c.casekey=cd.casekey)
	LEFT JOIN TABLEDESCRIPTORVALUE tdv on (cd.DESCRIPTORVALUEKEY=tdv.DESCRIPTORVALUEKEY 
											and tdv.DESCRIPTORKEY=4)
	JOIN GLOBALRELATION gr on (c.casekey=gr.relatedkey and gr.relationshipkey=104)
	join cases pc on (gr.parentkey=pc.casekey) -- pc: PriorityCase
	join caseevent cepc on (c.casekey=cepc.casekey and cepc.EVENTKEY=10) --cepc: CaseEventPriorityCase
	LEFT JOIN tablestatus ts on (c.statuskey=ts.statuskey)
	LEFT JOIN TABLEFILINGTYPE tf on (c.FILINGTYPEKEY=tf.FILINGTYPEKEY)
	LEFT JOIN tablecountry tc on (c.COUNTRYKEY=tc.COUNTRYKEY)
	left join casename cn106 on (c.casekey = cn106.casekey and cn106.nametypekey=106)
	left join name n106 on (n106.namekey = cn106.namekey)
	left join casename cn105 on (c.casekey = cn105.casekey and cn105.nametypekey=105)
	left join name n105 on (n105.namekey = cn105.namekey)
	left join casename cn101 on (c.casekey = cn101.casekey and cn101.nametypekey=101)
	left join name n101 on (n101.namekey = cn101.namekey)
	left join casename cn504 on (c.casekey = cn504.casekey and cn504.nametypekey=504)
	left join name n504 on (n504.namekey = cn504.namekey)
	left join casename cn12 on (c.casekey = cn12.casekey and cn12.nametypekey=12)
	left join name n12 on (n12.namekey = cn12.namekey)
	left join caseevent ce10 on (c.casekey=ce10.casekey and ce10.EVENTKEY=10)
	left join caseevent ce15 on (c.casekey=ce15.casekey and ce15.EVENTKEY=15)
	left join caseevent ce19 on (c.casekey=ce19.casekey and ce19.EVENTKEY=19)
	left join caseevent ce30 on (c.casekey=ce30.casekey and ce30.EVENTKEY=30)
	left join caseevent ce50 on (c.casekey=ce50.casekey and ce50.EVENTKEY=50)
WHERE
	c.propertytypekey = 1 
	and c.casetypekey = 1 
	and not c.statuskey in (3, 5811488)
	and c.casekey in 
		(select distinct casekey from auditcaselog inner join 
		auditlogtype at on at.auditlogtypekey = auditcaselog.auditlogtypekey 
		where auditlogcode like 'Case%')
) FTF_source-- end using
ON (FTF_excTarget.ASKSEND = '2' and FTF_excTarget.casekey = FTF_source.casekey and  (FTF_excTarget.TITLE <> FTF_source.title or FTF_excTarget.CASE_STATUS <> FTF_source.CASE_STATUS) )
WHEN MATCHED THEN
    update SET TITLE = FTF_source.title,CASE_STATUS = FTF_source.CASE_STATUS,ASKSEND = '3'  
-- exist :
--Todo merge field..
--WHEN NOT MATCHED THEN
-- INSERT (TITLE,CASE_STATUS,PRIMARY_CASE_NM,APPLICATION_TYPE,COUNTRY,casekey,DATEUPDATED,ASKSEND) VALUES (FTF_source.TITLE,FTF_source.CASE_STATUS,FTF_source.PRIMARY_CASE_NM,FTF_source.APPLICATION_TYPE,FTF_source.COUNTRY,FTF_source.casekey,sysdate,'0')
 ;


select TITLE,CASE_STATUS,PRIMARY_CASE_NM,APPLICATION_TYPE,COUNTRY,casekey,DATEUPDATED,ASKSEND from FTF_EXCHANGE ;