/* Formatted on 03/01/2017 11:58:36 (QP5 v5.300) */
DECLARE
    CURSOR c_cur
    IS
        SELECT *
          FROM FTF_EXCHANGE
         WHERE ASKSEND <> '2';

    v_count     INTEGER;
    v_casekey   INTEGER;
--    rowTarget   FTF_EXCHANGE%ROWTYPE;
    rowSource   FTF_EXCHANGE%ROWTYPE;
BEGIN
    FOR rowTarget IN c_cur
    LOOP

        SELECT DISTINCT
               SUBSTR (c.shorttitle, 0, 90) AS title,
               ts.STATUSDESCRIPTION         AS case_status,
               c.casereference              AS primary_case_nm,
               tf.FILINGTYPEDESCRIPTION     AS application_type,
               tc.COUNTRYDESCRIPTION        AS country,
               --REGEXP_REPLACE(c.casereference, '([^-]*[-]{1}[^-]*)-(.*)','\1') as case_family_nm,
                (SELECT MAX (casereference)
                   FROM cases
                  WHERE casekey = c.familykey)
                   AS case_family_nm,
               ce10.eventnumber             AS serial_number,
               ce10.eventdate               AS file_date,
               ce19.eventnumber             AS patent_number,
               ce19.eventdate               AS issue_date,
               ce50.EVENTDUEDATE            AS expire_date,
               cepc.eventdate               AS priority_date,
               ce30.eventdate               AS cat_generic_field1,
               tdv.DESCRIPTORKEY            AS generic_field2,
               (SELECT TO_CHAR (cpamemotechtools.blobtoclob (gml.longvalue))
                  FROM globalmedia gm, globalmedialob gml
                 WHERE     gm.globalmediakey = gml.globalmediakey
                       AND gm.parentkey = c.parentkey
                       AND gm.mediatypekey = 301)
                   AS comment_generic_note1,
               (SELECT TO_CHAR (cpamemotechtools.blobtoclob (gml.longvalue))
                  FROM globalmedia gm, globalmedialob gml
                 WHERE     gm.globalmediakey = gml.globalmediakey
                       AND gm.parentkey = c.parentkey
                       AND gm.mediatypekey = 23307323)
                   AS comment_generic_note2,
               n106.name                    AS inhatt,
               n105.name                    AS inhata,
               n101.name                    AS "group",
               n504.name                    AS tech_cluster,
               n12.name                     AS product,
               (SELECT LISTAGG (nametable.name, ';')
                           WITHIN GROUP (ORDER BY 1)
                  FROM casename
                       INNER JOIN name nametable
                           ON nametable.namekey = casename.namekey
                 WHERE nametypekey = 209 AND casekey = c.casekey)
                   AS inventors,
               ce15.eventdate               AS publication_date,
               ce15.eventnumber             AS case_publication_nm,
               c.casekey,
               c.familykey                  AS familykey,
               (SELECT MAX (eventcreationdate)
                  FROM caseevent
                 WHERE caseevent.casekey = c.casekey)
                   cust_last_update,
               SYSDATE                      AS dateUpdated,
               SYSDATE                      AS dateSended,
               '1'                          AS ASKSEND
          INTO rowSource
          FROM cases  c
               LEFT JOIN CASEDESCRIPTOR cd ON (c.casekey = cd.casekey)
               LEFT JOIN TABLEDESCRIPTORVALUE tdv
                   ON (    cd.DESCRIPTORVALUEKEY = tdv.DESCRIPTORVALUEKEY
                       AND tdv.DESCRIPTORKEY = 4)
               JOIN GLOBALRELATION gr
                   ON (c.casekey = gr.relatedkey AND gr.relationshipkey = 104)
               JOIN cases pc ON (gr.parentkey = pc.casekey) -- pc: PriorityCase
               JOIN caseevent cepc
                   ON (c.casekey = cepc.casekey AND cepc.EVENTKEY = 10) --cepc: CaseEventPriorityCase
               LEFT JOIN tablestatus ts ON (c.statuskey = ts.statuskey)
               LEFT JOIN TABLEFILINGTYPE tf
                   ON (c.FILINGTYPEKEY = tf.FILINGTYPEKEY)
               LEFT JOIN tablecountry tc ON (c.COUNTRYKEY = tc.COUNTRYKEY)
               LEFT JOIN casename cn106
                   ON (c.casekey = cn106.casekey AND cn106.nametypekey = 106)
               LEFT JOIN name n106 ON (n106.namekey = cn106.namekey)
               LEFT JOIN casename cn105
                   ON (c.casekey = cn105.casekey AND cn105.nametypekey = 105)
               LEFT JOIN name n105 ON (n105.namekey = cn105.namekey)
               LEFT JOIN casename cn101
                   ON (c.casekey = cn101.casekey AND cn101.nametypekey = 101)
               LEFT JOIN name n101 ON (n101.namekey = cn101.namekey)
               LEFT JOIN casename cn504
                   ON (c.casekey = cn504.casekey AND cn504.nametypekey = 504)
               LEFT JOIN name n504 ON (n504.namekey = cn504.namekey)
               LEFT JOIN casename cn12
                   ON (c.casekey = cn12.casekey AND cn12.nametypekey = 12)
               LEFT JOIN name n12 ON (n12.namekey = cn12.namekey)
               LEFT JOIN caseevent ce10
                   ON (c.casekey = ce10.casekey AND ce10.EVENTKEY = 10)
               LEFT JOIN caseevent ce15
                   ON (c.casekey = ce15.casekey AND ce15.EVENTKEY = 15)
               LEFT JOIN caseevent ce19
                   ON (c.casekey = ce19.casekey AND ce19.EVENTKEY = 19)
               LEFT JOIN caseevent ce30
                   ON (c.casekey = ce30.casekey AND ce30.EVENTKEY = 30)
               LEFT JOIN caseevent ce50
                   ON (c.casekey = ce50.casekey AND ce50.EVENTKEY = 50)
         WHERE c.casekey = rowTarget.casekey and rownum = 1 ;
    -- todo
    --Match ?



    END LOOP;
END;