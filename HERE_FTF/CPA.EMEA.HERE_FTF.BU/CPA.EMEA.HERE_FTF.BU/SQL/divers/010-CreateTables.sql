
-------------------------------------------------------
--AEC for HERE
--02 /01/2017
-------------------------------------------------------

BEGIN
   EXECUTE IMMEDIATE 'drop table FTF_EXCHA_PT CASCADE CONSTRAINTS';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

CREATE TABLE FTF_EXCHA_PT
(
  CASEKEY               INTEGER  NOT NULL,
  CASETYPE              VARCHAR2(256 BYTE),
  TITLE                 VARCHAR2(4000 BYTE),
  Country               VARCHAR2(2) DEFAULT 'xx',
  CaseReference         VARCHAR2(256),
  Status                VARCHAR2(256),
  StatusDate            date,
  APPLICATION_TYPE      VARCHAR2(256 BYTE) DEFAULT 'xx',
  CaseFamily            VARCHAR2(256 BYTE),
  FAMILYKEY              INTEGER,
  CUST_LAST_UPDATE       DATE,
  DATEUPDATED            DATE,
  DATESENDED             DATE,
  ASKSEND                CHAR(1 BYTE)
) ;
