

-- Fill In PTA
INSERT INTO FTF_EXCHA_PT 
select 
cases.casekey               as CASETYPE,
'PatentProject'             as CASETYPE,
cases.TITLE                 as TITLE,
TABLECOUNTRY.COUNTRYCODE    as Country,
CASES.CASEREFERENCE         as CaseReference,
curEvent.eventcode          as Status,                       
curcsEvent.EVENTDATE        as StatusDate,

from 
cases
inner join tablecountry on CASES.COUNTRYKEY = TABLECOUNTRY.COUNTRYKEY
inner join caseevent curcsEvent on curcsEvent.CASEEVENTKEY = CASES.CURRENTCASEEVENTKEY
inner join tableevent curEvent on curEvent.eventkey = curcsEvent.eventkey
where 
cases.PROPERTYTYPEKEY = 1 and cases.CASETYPEKEY = 1 
-- and is in FTF 
;


SELECT  
CASEKEY                 INTEGER  NOT NULL,
CASETYPE              VARCHAR2(256 BYTE),
TITLE                 VARCHAR2(4000 BYTE),
Country               VARCHAR2(2) DEFAULT 'xx',
CaseReference         VARCHAR2(256),
  Status                VARCHAR2(256),
  StatusDate            date,
  APPLICATION_TYPE      VARCHAR2(256 BYTE) DEFAULT 'xx',
  CaseFamily            VARCHAR2(256 BYTE),
  FAMILYKEY              INTEGER,
  CUST_LAST_UPDATE       DATE,
  DATEUPDATED            DATE,
  DATESENDED             DATE,
  ASKSEND                CHAR(1 BYTE)