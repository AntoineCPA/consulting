SELECT distinct
	substr(c.shorttitle,0,90) as title,
	ts.STATUSDESCRIPTION as case_status,
	c.casereference as primary_case_nm,
	tf.FILINGTYPEDESCRIPTION as application_type,
	tc.COUNTRYDESCRIPTION as country,
	REGEXP_REPLACE(c.casereference, '([^-]*[-]{1}[^-]*)-(.*)','\1') as case_family_nm, 
	ce10.eventnumber as serial_number,
	ce10.eventdate as file_date,
	ce19.eventnumber as patent_number,
	ce19.eventdate as issue_date,
	ce50.EVENTDUEDATE as expire_date,
	cepc.eventdate as priority_date,
	ce30.eventdate as cat_generic_field1,
	tdv.DESCRIPTORKEY as generic_field2, 
	(	select  cpamemotechtools.blobtoclob(c.longvalue) 
		from globalmedia gm,globalmedialob gml 
		where gm.globalmediakey=gml.globalmediakey and gm.parentkey=c.parentkey 
		and gm.mediatypekey=301) as comment_generic_note1,
	(	select  cpamemotechtools.blobtoclob(c.longvalue) 
		from globalmedia gm,globalmedialob gml 
		where gm.globalmediakey=gml.globalmediakey and gm.parentkey=c.parentkey 
		and gm.mediatypekey=23307323) as comment_generic_note2,
	n106.name as inhatt,
	n105.name as inhata,
	n101.name as "group",
	n504.name as tech_cluster,
	n12.name as product,
	(select listagg(nametable.name, ';') within group (order by 1) 
        from casename inner join name nametable on nametable.namekey = casename.namekey 
        where nametypekey=209	and casekey=c.casekey) as inventors,
	ce15.eventdate as publication_date,
	ce15.eventnumber as case_publication_nm,
	c.casekey,
	c.casekey as familykey,
	(select max(eventcreationdate) from caseevent where caseevent.casekey = c.casekey ) cust_last_update
FROM
	cases c
	LEFT JOIN CASEDESCRIPTOR cd on (c.casekey=cd.casekey)
	LEFT JOIN TABLEDESCRIPTORVALUE tdv on (cd.DESCRIPTORVALUEKEY=tdv.DESCRIPTORVALUEKEY 
											and tdv.DESCRIPTORKEY=4)
	JOIN GLOBALRELATION gr on (c.casekey=gr.relatedkey and gr.relationshipkey=104)
	join cases pc on (gr.parentkey=pc.casekey) -- pc: PriorityCase
	join caseevent cepc on (c.casekey=cepc.casekey and cepc.EVENTKEY=10) --cepc: CaseEventPriorityCase
	LEFT JOIN tablestatus ts on (c.statuskey=ts.statuskey)
	LEFT JOIN TABLEFILINGTYPE tf on (c.FILINGTYPEKEY=tf.FILINGTYPEKEY)
	LEFT JOIN tablecountry tc on (c.COUNTRYKEY=tc.COUNTRYKEY)
	left join casename cn106 on (c.casekey = cn106.casekey and cn106.nametypekey=106)
	left join name n106 on (n106.namekey = cn106.namekey)
	left join casename cn105 on (c.casekey = cn105.casekey and cn105.nametypekey=105)
	left join name n105 on (n105.namekey = cn105.namekey)
	left join casename cn101 on (c.casekey = cn101.casekey and cn101.nametypekey=101)
	left join name n101 on (n101.namekey = cn101.namekey)
	left join casename cn504 on (c.casekey = cn504.casekey and cn504.nametypekey=504)
	left join name n504 on (n504.namekey = cn504.namekey)
	left join casename cn12 on (c.casekey = cn12.casekey and cn12.nametypekey=12)
	left join name n12 on (n12.namekey = cn12.namekey)
	left join caseevent ce10 on (c.casekey=ce10.casekey and ce10.EVENTKEY=10)
	left join caseevent ce15 on (c.casekey=ce15.casekey and ce15.EVENTKEY=15)
	left join caseevent ce19 on (c.casekey=ce19.casekey and ce19.EVENTKEY=19)
	left join caseevent ce30 on (c.casekey=ce30.casekey and ce30.EVENTKEY=30)
	left join caseevent ce50 on (c.casekey=ce50.casekey and ce50.EVENTKEY=50)
WHERE
	c.propertytypekey = 1 
	and c.casetypekey = 1 
	and not c.statuskey in (3, 5811488)
	and c.casekey in 
		(select distinct casekey from auditcaselog inner join 
		auditlogtype at on at.auditlogtypekey = auditcaselog.auditlogtypekey 
		where auditlogcode like 'Case%')
	--and pca.ConfirmationNumber = 'FTF'
;