/* Formatted on 23/02/2017 13:24:24 (QP5 v5.300) */
CREATE OR REPLACE PROCEDURE rp_OnFTF (
    psUserIdentityKey   IN     VARCHAR2,
    psCulture           IN     VARCHAR2,
    psXmlParameters     IN     VARCHAR2,
    pcCurList1             OUT CPAmemotechType.RefCur)
IS
    RELEASE_5_3_3848   CONSTANT BOOLEAN := FALSE;

    xpParams                    xmlparser.Parser;
    xnParams                    xmldom.DOMNode;

    nCaseEvaluationKey          INTEGER;
    sQuery                      VARCHAR2 (4000);
    sDomainRestriction          VARCHAR2 (4000);
BEGIN
    OPEN pcCurList1 FOR
        SELECT DISTINCT
               SUBSTR (c.shorttitle, 0, 90)           AS title,
               NVL (ts.STATUSDESCRIPTION, 'XX')       AS case_status,
               c.casereference                        AS primary_case_nm,
               NVL (tf.FILINGTYPEDESCRIPTION, 'XX')   AS application_type,
               NVL (tc.COUNTRYDESCRIPTION, 'XX')      AS country,
               NVL ( (SELECT f.CASEREFERENCE
                        FROM cases f
                       WHERE f.casekey = c.familykey),
                    c.CASEREFERENCE)
                   AS case_family_nm,
               ce15.eventnumber                       AS generic_field1, --publication number
               ce25.eventnumber                       AS generic_field2, --Grant number
               CaseFunctions.GetFirstPriority (1, '', c.casekey) AS generic_field3,
               ce10.eventnumber                       AS serial_number,
               TO_CHAR (ce10.eventdate, 'MM/DD/YYYY') AS file_date,
               TO_CHAR (CaseFunctions.GetFirstPriorityDate (c.CaseKey),
                        'MM/DD/YYYY')
                   AS priority_date,
               ce25.eventnumber                       AS patent_number,
               TO_CHAR (ce15.eventdate, 'MM/DD/YYYY') AS publication_date,
               ce15.eventnumber                       AS case_publication_nm,
               (SELECT namemanagement.displaydetailedname (cn.namekey)
                  FROM casename cn
                 WHERE cn.nametypekey = 106 AND cn.casekey = c.casekey)
                   AS inhatt,
               (SELECT namemanagement.displaydetailedname (cn.namekey)
                  FROM casename cn
                 WHERE cn.nametypekey = 100 AND cn.casekey = c.casekey)
                   AS outcou,
               cn100.ReferenceNo
                   AS outside_counsel_docket_nm1,
               TO_CHAR (NVL ( (SELECT MAX (acl.LOGDATE)
                                 FROM AUDITCASELOG acl
                                WHERE acl.CASEKEY = c.casekey),
                             (SELECT MAX (ce.EVENTCREATIONDATE)
                                FROM CASEEVENT ce
                               WHERE ce.CASEKEY = c.casekey)),
                        'MM/DD/YYYY')
                   AS cust_last_update
          FROM cases  c
               LEFT JOIN tablestatus ts ON (c.statuskey = ts.statuskey)
               LEFT JOIN TABLEFILINGTYPE tf
                   ON (c.FILINGTYPEKEY = tf.FILINGTYPEKEY)
               LEFT JOIN tablecountry tc ON (c.COUNTRYKEY = tc.COUNTRYKEY)
               LEFT JOIN caseevent ce10
                   ON (c.casekey = ce10.casekey AND ce10.EVENTKEY = 10)
               LEFT JOIN caseevent ce15
                   ON (c.casekey = ce15.casekey AND ce15.EVENTKEY = 15)
               LEFT JOIN caseevent ce19
                   ON (c.casekey = ce19.casekey AND ce19.EVENTKEY = 19)
               LEFT JOIN caseevent ce25
                   ON (c.casekey = ce25.casekey AND ce25.eventkey = 25) --eventkey=25 = Grant
               LEFT JOIN casename cn100
                   ON (c.casekey = cn100.casekey AND cn100.NAMETYPEKEY = 100);
--select * from FTF_EXCHANGE where ASKSEND = '0' ;

--update FTF_EXCHANGE set ASKSEND = '1',dateSended = sysdate where  ASKSEND = '0' ;
--commit;
--and pca.ConfirmationNumber = 'FTF'

END rp_OnFTF;
/