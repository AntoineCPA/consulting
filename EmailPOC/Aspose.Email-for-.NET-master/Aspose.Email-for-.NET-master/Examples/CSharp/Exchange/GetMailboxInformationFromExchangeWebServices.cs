﻿using System;
using Aspose.Email.Exchange;

/*
This project uses Automatic Package Restore feature of NuGet to resolve Aspose.Email for .NET API reference 
when the project is build. Please check https://Docs.nuget.org/consume/nuget-faq for more information. 
If you do not wish to use NuGet, you can manually download Aspose.Email for .NET API from http://www.aspose.com/downloads, 
install it and then add its reference to this project. For any issues, questions or suggestions 
please feel free to contact us using http://www.aspose.com/community/forums/default.aspx
*/

namespace Aspose.Email.Examples.CSharp.Email.Exchange
{
    class GetMailboxInformationFromExchangeWebServices
    {
        public static void Run()
        {
            // The path to the File directory.
            // ExStart:GetMailboxInformationFromExchangeWebServices 
            // Create instance of EWSClient class by giving credentials
            try
            {
                // Basic :
                //https://outlook.office365.com/ews/exchange.asmx

                // https://outlook.office365.com/owa/cpaglobal.com/exchange.asmx
                //https://outlook.office365.com/owa/cpaglobal.com/exchange.asmx
                //https://outlook.office365.com/mapi/emsmdb/?MailboxId=9cdea041-f675-4590-aff4-e5a7698a3f71@cpaglobal.com



                IEWSClient client = EWSClient.GetEWSClient(@"https://outlook.office365.com/ews/exchange.asmx", "AEckly@cpaglobal.com", "16Aeckaeck!");
                //IEWSClient client = EWSClient.GetEWSClient("https://outlook.office365.com/owa/cpaglobal.com/exchange.asmx", "aeckly@cpaglobal.com", "16Aeckaeck!","int");

            // Get mailbox size, exchange mailbox info, Mailbox and Inbox folder URI
            Console.WriteLine("Mailbox size: " + client.GetMailboxSize() + " bytes");
            ExchangeMailboxInfo mailboxInfo = client.GetMailboxInfo();
            Console.WriteLine("Mailbox URI: " + mailboxInfo.MailboxUri);
            Console.WriteLine("Inbox folder URI: " + mailboxInfo.InboxUri);
            Console.WriteLine("Sent Items URI: " + mailboxInfo.SentItemsUri);
            Console.WriteLine("Drafts folder URI: " + mailboxInfo.DraftsUri);
            // ExEnd:GetMailboxInformationFromExchangeWebServices
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }
    }
}